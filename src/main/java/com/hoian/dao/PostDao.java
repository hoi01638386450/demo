package com.hoian.dao;

import java.util.List;

import com.hoian.entity.Post;

public interface PostDao {
public List<Post> getAllPost();
	
	public void addPost(Post post);
	
	public void deletePost(Post post);
	
	public void updatePost(Post post);
	
	public List<Post> getPostByIdUser(int id);
	
	
}
