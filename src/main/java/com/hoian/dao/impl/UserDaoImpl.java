package com.hoian.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hoian.entity.User;
import org.springframework.stereotype.Repository;

import com.hoian.dao.UserDao;

@Repository
public class UserDaoImpl implements UserDao {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public List<com.hoian.entity.User> getAllUser() {
		String jql = "SELECT e FROM User e";
		return entityManager.createQuery(jql, User.class).getResultList();
	}

	public void addUser(User user) {
		entityManager.persist(user);
		
	}

	public void deleteUser(User user) {
		entityManager.remove(user);
		
	}

	public void updateUser(User user) {
		entityManager.merge(user);
		
	}

	public User getUser(int id) {
		return entityManager.find(User.class, id);
	}

	public User getUserByUserName(String username) {
		String jql = "SELECT e FROM User e WHERE e.username = :abcefghg";
		return entityManager.createQuery(jql, User.class).setParameter("abcefghg", username).getSingleResult();
	}

}
