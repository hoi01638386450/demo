package com.hoian.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.hoian.dao.PostDao;
import com.hoian.entity.Post;
@Repository
public class PostDaoImpl implements PostDao {
	@PersistenceContext
	EntityManager entityManager;
	@Override
	public List<Post> getAllPost() {
		String jql = "SELECT p FROM Post p";
		return entityManager.createQuery(jql, Post.class).getResultList();
	}

	@Override
	public void addPost(Post post) {
		entityManager.persist(post);
		
	}

	@Override
	public void deletePost(Post post) {
		entityManager.remove(post);
		
	}

	@Override
	public void updatePost(Post post) {
		entityManager.merge(post);
		
	}

	@Override
	public List<Post> getPostByIdUser(int user_id) {
		String jql = "select p from Post p Join p.user u on u.id = :abc";
		return entityManager.createQuery(jql, Post.class).setParameter("abc", user_id).getResultList();
	}

	
}
