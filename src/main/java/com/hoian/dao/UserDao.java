package com.hoian.dao;

import java.util.List;

import com.hoian.entity.User;



public interface UserDao {

	public List<User> getAllUser();
	
	public void addUser(User user);
	
	public void deleteUser(User user);
	
	public void updateUser(User user);
	
	public User getUser(int id);
	
	public User getUserByUserName(String username);
}
