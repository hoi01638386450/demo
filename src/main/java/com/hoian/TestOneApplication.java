package com.hoian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.hoian.service.Impl.UserLoginServiceImpl;

@SpringBootApplication
public class TestOneApplication extends WebSecurityConfigurerAdapter {
	@Autowired
	UserLoginServiceImpl loginServiceImpl;
	public static void main(String[] args) {
		SpringApplication.run(TestOneApplication.class, args);
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/users/create").permitAll().antMatchers("/users/delete")
	.hasAnyRole("ADMIN").anyRequest().authenticated().and().httpBasic();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(loginServiceImpl).passwordEncoder(new BCryptPasswordEncoder());

	}
}
