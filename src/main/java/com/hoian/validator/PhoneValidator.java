package com.hoian.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, String>  {

	public boolean isValid(String phone, ConstraintValidatorContext context) {
		if(phone !=null) {
			
		String sdt="^0\\d{9}";
		boolean matcher= Pattern.matches(sdt, phone);
		return matcher;
	
		}
		return false;
	}
	

}
