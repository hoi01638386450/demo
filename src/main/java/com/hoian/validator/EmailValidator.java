package com.hoian.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Email;

public class EmailValidator implements ConstraintValidator<Email, String> {

	public boolean isValid(String email, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		if (email == null) {
			return false;
		}
		String a="^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";
		boolean matcher=Pattern.matches(a, email);
		return matcher;
	}

}
