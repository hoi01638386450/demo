package com.hoian.model;

import java.time.LocalDate;

import com.hoian.entity.Post;
import com.hoian.entity.User;

public class CommentDTO {
	private int id;

	private int parent_id;
	
	private Post post_id;
	private User user_id;
	private String content;
	private Boolean status;
	private LocalDate created_at;
	private LocalDate modified_at;

	public CommentDTO(int id, int parent_id, Post post_id, User user_id, String content, Boolean status,
			LocalDate created_at, LocalDate modified_at) {
		super();
		this.id = id;
		this.parent_id = parent_id;
		this.post_id = post_id;
		this.user_id = user_id;
		this.content = content;
		this.status = status;
		this.created_at = created_at;
		this.modified_at = modified_at;
	}

	public CommentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParent_id() {
		return parent_id;
	}

	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}

	public Post getPost_id() {
		return post_id;
	}

	public void setPost_id(Post post_id) {
		this.post_id = post_id;
	}

	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public LocalDate getCreated_at() {
		return created_at;
	}

	public void setCreated_at(LocalDate created_at) {
		this.created_at = created_at;
	}

	public LocalDate getModified_at() {
		return modified_at;
	}

	public void setModified_at(LocalDate modified_at) {
		this.modified_at = modified_at;
	}
	
}
