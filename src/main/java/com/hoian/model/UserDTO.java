package com.hoian.model;

import java.time.LocalDate;
import java.util.Date;

public class UserDTO {
	private int id;
	
	private String username;
	private String password;
	
	private String fullname;

	
	private String email;

	private String phone;
	private String avatar;
	private Date birthday;
//	private boolean status;
	private LocalDate created_at;
	private LocalDate modified_at;
	private String token;
	private String role;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
//	public boolean isStatus() {
//		return status;
//	}
//	public void setStatus(boolean status) {
//		this.status = status;
//	}
	public LocalDate getCreated_at() {
		return created_at;
	}
	public void setCreated_at(LocalDate created_at) {
		this.created_at = created_at;
	}
	public LocalDate getModified_at() {
		return modified_at;
	}
	public void setModified_at(LocalDate modified_at) {
		this.modified_at = modified_at;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public UserDTO(int id, String username, String password, String fullname, String email, String phone, String avatar,
			Date birthday, boolean status, LocalDate created_at, LocalDate modified_at, String token, String role) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.avatar = avatar;
		this.birthday = birthday;
//		this.status = status;
		this.created_at = created_at;
		this.modified_at = modified_at;
		this.token = token;
		this.role = role;
	}
	public UserDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
