package com.hoian.model;

import java.time.LocalDate;
import java.util.List;

import com.hoian.entity.Comments;
import com.hoian.entity.User;


public class PostDTO {
	private int id;
	
	private User user;

	private List<Comments> listComment;

	private String title;
	private String slug;
	private String summary;
	private String content;
	private String image;
	private String meta_title;
	private String meta_keywords;
	private String meta_description;
	private int num_view;
	private Boolean status;
	private LocalDate created_at;
	private LocalDate modified_at;
	public PostDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PostDTO(int id, User user, List<Comments> listComment, String title, String slug, String summary,
			String content, String image, String meta_title, String meta_keywords, String meta_description,
			int num_view, Boolean status, LocalDate created_at, LocalDate modified_at) {
		super();
		this.id = id;
		this.user = user;
		this.listComment = listComment;
		this.title = title;
		this.slug = slug;
		this.summary = summary;
		this.content = content;
		this.image = image;
		this.meta_title = meta_title;
		this.meta_keywords = meta_keywords;
		this.meta_description = meta_description;
		this.num_view = num_view;
		this.status = status;
		this.created_at = created_at;
		this.modified_at = modified_at;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Comments> getListComment() {
		return listComment;
	}
	public void setListComment(List<Comments> listComment) {
		this.listComment = listComment;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getMeta_title() {
		return meta_title;
	}
	public void setMeta_title(String meta_title) {
		this.meta_title = meta_title;
	}
	public String getMeta_keywords() {
		return meta_keywords;
	}
	public void setMeta_keywords(String meta_keywords) {
		this.meta_keywords = meta_keywords;
	}
	public String getMeta_description() {
		return meta_description;
	}
	public void setMeta_description(String meta_description) {
		this.meta_description = meta_description;
	}
	public int getNum_view() {
		return num_view;
	}
	public void setNum_view(int num_view) {
		this.num_view = num_view;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public LocalDate getCreated_at() {
		return created_at;
	}
	public void setCreated_at(LocalDate created_at) {
		this.created_at = created_at;
	}
	public LocalDate getModified_at() {
		return modified_at;
	}
	public void setModified_at(LocalDate modified_at) {
		this.modified_at = modified_at;
	}
	
}
