package com.hoian.service;

import java.util.List;

import com.hoian.entity.Post;
import com.hoian.model.PostDTO;

public interface PostService {
	public List<PostDTO> getAllPost();

	public Post addPost(PostDTO postDTO);
	
	public void deletetPost(int id);
	
	public void updatePost(PostDTO postDTO);
	
	public List<PostDTO> getPostByIdUser(int user);
}
