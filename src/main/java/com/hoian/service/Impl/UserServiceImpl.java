package com.hoian.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hoian.dao.UserDao;
import com.hoian.entity.User;
import com.hoian.model.UserDTO;
import com.hoian.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	public List<UserDTO> getAllUsers() {
		List<User> users = userDao.getAllUser();

		List<UserDTO> dtos = new ArrayList<UserDTO>();
		for (User e : users) {
			UserDTO dto = new UserDTO();
			dto.setId(e.getId());
			dto.setUsername(e.getUsername());
			dto.setPassword(e.getPassword());
			dto.setFullname(e.getFullname());
//			dto.setStatus(e.isStatus());
			dto.setEmail(e.getEmail());
			dto.setPhone(e.getPhone());
			dto.setAvatar(e.getAvatar());
			dto.setBirthday(e.getBirthday());
			dto.setCreated_at(e.getCreated_at());
			dto.setModified_at(e.getModified_at());
			dtos.add(dto);
		}

		return dtos;
	}

	public User addUser(UserDTO user) {

		User e = new User();
		e.setFullname(user.getFullname());
		e.setUsername(user.getUsername());
//		e.setStatus(user.isStatus());
		e.setEmail(user.getEmail());
		e.setPhone(user.getPhone());
		e.setAvatar(user.getAvatar());
		e.setBirthday(user.getBirthday());
		e.setCreated_at(user.getCreated_at());
		e.setModified_at(user.getModified_at());
		BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
		e.setPassword(bc.encode(user.getPassword()));
		
		e.setRole(user.getRole());
		userDao.addUser(e);
		return e;
	}

	public void deletetUser(int id) {
		User user = userDao.getUser(id);
		if (user != null) {
			userDao.deleteUser(user);
		}

	}

	public void updateUser(UserDTO userDTO) {
		User user2 = userDao.getUser(userDTO.getId());
		if (user2 != null) {
			user2.setFullname(userDTO.getFullname());
			user2.setUsername(userDTO.getUsername());
			user2.setPassword(userDTO.getPassword());
			user2.setEmail(userDTO.getEmail());
//			user2.setStatus(userDTO.isStatus());
			user2.setPhone(userDTO.getPhone());
			user2.setAvatar(userDTO.getAvatar());
			user2.setBirthday(userDTO.getBirthday());
			user2.setCreated_at(userDTO.getCreated_at());
			user2.setModified_at(userDTO.getModified_at());
		}
		userDao.updateUser(user2);
	}

	public UserDTO getUser(int id) {
		User user = userDao.getUser(id);
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setFullname(user.getFullname());
		userDTO.setUsername(user.getUsername());
		userDTO.setPassword(user.getPassword());
		userDTO.setEmail(user.getEmail());
//		userDTO.setStatus(user.isStatus());
		userDTO.setPhone(user.getPhone());
		userDTO.setAvatar(user.getAvatar());
		userDTO.setBirthday(user.getBirthday());
		userDTO.setCreated_at(user.getCreated_at());
		userDTO.setModified_at(user.getModified_at());
		return userDTO;
	}

}
