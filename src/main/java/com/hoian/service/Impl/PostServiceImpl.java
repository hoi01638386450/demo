package com.hoian.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hoian.dao.PostDao;
import com.hoian.entity.Post;
import com.hoian.model.PostDTO;
import com.hoian.service.PostService;

public class PostServiceImpl implements PostService {

	@Autowired
	private PostDao postDao;

	@Override
	public List<PostDTO> getAllPost() {

		List<Post> posts = postDao.getAllPost();
		List<PostDTO> postDTOs = new ArrayList<PostDTO>();
		for (Post p : posts) {
			PostDTO postDTO = new PostDTO();
			postDTO.setId(p.getId());
			postDTO.setUser(p.getUser());
			postDTO.setListComment(p.getListComment());
			postDTO.setTitle(p.getTitle());
			postDTO.setSlug(p.getSlug());
			postDTO.setSummary(p.getSummary());
			postDTO.setContent(p.getContent());
			postDTO.setImage(p.getImage());
			postDTO.setMeta_title(p.getMeta_title());
			postDTO.setMeta_description(p.getMeta_description());
			postDTO.setMeta_keywords(p.getMeta_keywords());
			postDTO.setNum_view(p.getNum_view());
			postDTO.setStatus(p.getStatus());
			postDTO.setCreated_at(p.getCreated_at());
			postDTO.setModified_at(p.getModified_at());
			postDTOs.add(postDTO);
		}
		return postDTOs;
	}

	@Override
	public Post addPost(PostDTO postDTO) {
		Post post = new Post();
		post.setId(postDTO.getId());

		post.setUser(postDTO.getUser());

		post.setTitle(postDTO.getTitle());
		post.setSlug(postDTO.getSlug());
		post.setSummary(postDTO.getSummary());
		post.setContent(postDTO.getContent());
		post.setImage(postDTO.getImage());
		post.setMeta_title(postDTO.getMeta_title());
		post.setMeta_description(postDTO.getMeta_description());
		post.setMeta_keywords(postDTO.getMeta_keywords());
		post.setNum_view(postDTO.getNum_view());
		post.setStatus(postDTO.getStatus());
		post.setCreated_at(postDTO.getCreated_at());
		post.setModified_at(postDTO.getModified_at());
		postDao.addPost(post);

		return post;
	}

	@Override
	public void deletetPost(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePost(PostDTO postDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<PostDTO> getPostByIdUser(int user) {
		List<Post> posts = postDao.getPostByIdUser(user);
		List<PostDTO> postDTOs = new ArrayList<PostDTO>();
		for (Post p : posts) {
			PostDTO postDTO = new PostDTO();

			postDTO.setId(p.getId());
			postDTO.setUser(p.getUser());
			postDTO.setListComment(p.getListComment());
			postDTO.setTitle(p.getTitle());
			postDTO.setSlug(p.getSlug());
			postDTO.setSummary(p.getSummary());
			postDTO.setContent(p.getContent());
			postDTO.setImage(p.getImage());
			postDTO.setMeta_title(p.getMeta_title());
			postDTO.setMeta_description(p.getMeta_description());
			postDTO.setMeta_keywords(p.getMeta_keywords());
			postDTO.setNum_view(p.getNum_view());
			postDTO.setStatus(p.getStatus());
			postDTO.setCreated_at(p.getCreated_at());
			postDTO.setModified_at(p.getModified_at());
			postDTOs.add(postDTO);
		}

		return postDTOs;
	}

}
