package com.hoian.service;

import java.util.List;

import com.hoian.model.UserDTO;
import com.hoian.entity.User;;

public interface UserService {
	public List<UserDTO> getAllUsers();

	public User addUser(UserDTO user);
	
	public void deletetUser(int id);
	
	public void updateUser(UserDTO userDTO);
	
	public UserDTO getUser(int id);
}
