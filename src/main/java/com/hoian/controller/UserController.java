package com.hoian.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hoian.entity.User;
import com.hoian.model.UserDTO;
import com.hoian.service.UserService;
@RestController
public class UserController {

	@Autowired
	private UserService userService;


	
	@GetMapping(value = "/users")
	public List<UserDTO> users(HttpServletRequest request ) {
		List<UserDTO> users =userService.getAllUsers();
		
		return users ;
	}
	@GetMapping(value = "/users/{id}")
	public UserDTO userById(@PathVariable(name="id") int userId ) {
		UserDTO user =userService.getUser(userId);
		return user; 
	}

	@PostMapping(value = "/users/create")
	public User addUser(@RequestBody UserDTO user) {
		return userService.addUser(user);
		

	}
	@DeleteMapping(value = "/users/delete/{id}")
	public void deleteUser( @PathVariable("id") int id) {
		userService.deletetUser(id);
		}
	@PutMapping(value = "/users/update/{id}")
	public void updateUser( @RequestBody UserDTO user) {
		userService.updateUser(user);
		}

	
}
