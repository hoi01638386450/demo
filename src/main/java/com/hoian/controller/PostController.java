package com.hoian.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hoian.model.PostDTO;
import com.hoian.service.PostService;

@RestController
public class PostController {
	@Autowired
	private PostService postService;

	@GetMapping(value = "/posts")
	public List<PostDTO> posts(HttpServletRequest request) {
		List<PostDTO> postDTOs = postService.getAllPost();

		return postDTOs;
	}

	@GetMapping(value = "/posts/{id}")
	public List<PostDTO> postById(@PathVariable(name = "id") int userId) {
		List<PostDTO> postDTOs = postService.getPostByIdUser(userId);
		return postDTOs;
	}
}
